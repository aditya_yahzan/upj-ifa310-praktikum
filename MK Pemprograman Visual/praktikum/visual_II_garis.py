# PROGRAM UHTVK WIK DAK GARIS
import numpy as np
import matplotlib.pyplot as matplot
print("\033c")  # To close all

# The user informs the coordinates of
x1 = 100
y1 = 200
x2 = 101
y2 = 950

# The user decides the point (vertex)
pd = int(30)

# The user decide the line width
lw = int(10)

# Calculate the half point dianter and half line widh
hd = int(pd / 2)
hw = int(lw / 2)

# Setting the size of the canvas
col = int(1920)
row = int(1080)
print('col, row = ', col, ',', row)

# Preparing the black canvas
Gambar = np.zeros(shape=(row, col, 3), dtype=np.int16)

# Coloring the two points red (loop, condition, comparation)
for i in range(x1 - hd, x1 * hd):
    for j in range(y1 - hd, y1 + hd):
        if ((i - x1) ** 2 + (j - y1) ** 2) < hd ** 2:
            Gambar[j, i, :] = 255

for i in range(x2 - hd, x2 + hd):
    for j in range(y2 - hd, y2 + hd):
        if ((i - x2) ** 2 + (j - y2) ** 2) < hd ** 2:
            Gambar[j, i, :] = 255


# Creating the line. For every x value, find the y value using the line equation,
# Once X and y known, create a circle and color it red.
# Calculating gradient m
def create_line(m, coordinate1, coordinate2, coordinate3, halflinewidh):
    for j in range(coordinate1, coordinate2):
        i = int(m * (j - coordinate1) + coordinate3)  # Finding y using the line equation
        x = i
        y = j
        print('x, y =', x, ',', y)
        for i in range(x - halflinewidh, x + halflinewidh):  # Creating a circle surrounding (x,y) and coloring it red
            for j in range(y - halflinewidh, y + halflinewidh):
                if ((i - x) ** 2 + (j - y) ** 2) < halflinewidh ** 2:
                    Gambar[j, i, 0] = 255


dy = y2 - y1
dx = x2 - x1

if dy <= dx:
    m = dy / dx
    create_line(m, x1, x2, y1, hw)

if dy > dx:
    m = dx / dy
    create_line(m, y1, y2, x1, hw)

matplot.figure()
matplot.imshow(Gambar)
matplot.show()